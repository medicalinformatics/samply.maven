# Samply Maven

## How to use the internal MITRO maven repository

Maven uses several repositories:

1. $HOME/.m2/repository
2. the active repositories in your settings.xml and pom.xml
3. the central Maven Repository

If maven searches for artifacts this list of repository is searched in this
particular order. The first artifact that matches the requirements is used.

MITRO hosts a Sonatype Nexus Maven Repository on

```
https://nexus.mitro.dkfz.de
```

This server is only accesible for certain network. If you want to
access it from home, you must connect to one of those networks. For users without access to one of those networks,
see [this page](https://maven.mitro.dkfz.de/external.html) for instructions.

```xml

<?xml version="1.0" encoding="UTF-8"?>
<settings
    xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd"
    xmlns="http://maven.apache.org/SETTINGS/1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <!-- Delete the proxy settings, if you do not use the DKFZ Proxy -->
    <proxies>
        <proxy>
            <id>myproxy</id>
            <active>true</active>
            <protocol>http</protocol>
            <host>www-int2.inet.dkfz-heidelberg.de</host>
            <port>3128</port>
            <username/>
            <password/>
            <nonProxyHosts>*.dkfz-heidelberg.de,*.dkfz.de</nonProxyHosts>
        </proxy>
    </proxies>

    <!-- Delete the mirror settings, if you do not want to use the DKFZ mirror -->
    <mirrors>
        <mirror>
            <mirrorOf>central</mirrorOf>
            <name>Central Repository Mirror</name>
            <url>http://nexus.mitro.dkfz.de/content/repositories/central/</url>
            <id>central-mirror</id>
        </mirror>
    </mirrors>

    <profiles>
        <profile>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <repositories>
                <repository>
                    <releases>
                        <enabled>false</enabled>
                    </releases>
                    <snapshots>
                        <enabled>true</enabled>
                    </snapshots>
                    <id>oss-snapshots</id>
                    <url>https://nexus.mitro.dkfz.de/content/repositories/oss-snapshots</url>
                </repository>
                <repository>
                    <releases>
                        <enabled>false</enabled>
                    </releases>
                    <snapshots>
                        <enabled>true</enabled>
                    </snapshots>
                    <id>snapshots</id>
                    <url>https://nexus.mitro.dkfz.de/content/repositories/snapshots</url>
                </repository>

                <repository>
                    <releases>
                        <enabled>true</enabled>
                    </releases>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                    <id>oss-releases</id>
                    <url>https://nexus.mitro.dkfz.de/content/repositories/oss-releases</url>
                </repository>
                <repository>
                    <releases>
                        <enabled>true</enabled>
                    </releases>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                    <id>releases</id>
                    <url>https://nexus.mitro.dkfz.de/content/repositories/releases</url>
                </repository>
            </repositories>
        </profile>
    </profiles>
</settings>
```

If you want to upload your artifacts to that maven repository, create a new
"encrypted" master password with maven: `mvn --encrypt-master-password
$SECURE_PASSWORD` (The newest maven version asks for your password, the old
versions require you to enter it as an argument). You do not need to remember
this password. Use the output it generates to create the file
`$HOME/.m2/settings-security.xml`:

```xml
<settingsSecurity>
    <master>$OUTPUT</master>
</settingsSecurity>
```

Now you can encrypt other passwords (e.g. the password for your Nexus account /
Active Directory account) with `mvn --encrypt-password $AD_PASSWORD` and use
those encrypted passwords in your settings.xml (replace weingarp with your DKFZ AD
account name):

```xml
<settings ...>

    <servers>
        <server>
            <id>releases</id>
            <username>weingarp</username>
            <password>$ENCRYPTED</password>
        </server>
        <server>
            <id>snapshots</id>
            <username>weingarp</username>
            <password>$ENCRYPTED</password>
        </server>
        <server>
            <id>mitro-maven</id>
            <username>weingarp</username>
            <password>$ENCRYPTED</password>
        </server>
    </servers>

    <profiles>
      ....
    </profiles>
</settings>
```

Now you have configured maven to use authenication if required but your projects
need to know where to deploy the artifacts. You can do this by either adding the
following tags in your pom.xml (for closed source projects. For open source projects use `oss-releases` and `oss-snapshots`):

```xml
  <distributionManagement>
    <repository>
      <id>mitro-maven</id>
      <url>https://nexus.mitro.dkfz.de/content/repositories/releases</url>
    </repository>
    <snapshotRepository>
      <id>mitro-maven</id>
      <url>https://nexus.mitro.dkfz.de/content/repositories/snapshots</url>
    </snapshotRepository>
  </distributionManagement>
```

Another way is to use this `pom.xml` as a parent, inheriting all dependency
versions, but keep in mind that using this parent pom.xml may have sideeffects:

```xml
<parent>
    <groupId>de.samply</groupId>
    <artifactId>parent</artifactId>
    <version>${VERSION}</version>
</parent>
```
