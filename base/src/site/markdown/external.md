# Samply Maven

## How to use the public MITRO maven repository

Maven uses several repositories:

1. $HOME/.m2/repository
2. the active repositories in your settings.xml and pom.xml
3. the central Maven Repository

If maven searches for artifacts this list of repository is searched in this
particular order. The first artifact that matches the requirements is used.

MITRO hosts a public Maven Repository on

```
https://maven.mitro.dkfz.de/
```

Copy and paste the following content into your `settings.xml` file (`$HOME/.m2/settings.xml`).

```xml

<?xml version="1.0" encoding="UTF-8"?>
<settings
    xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd"
    xmlns="http://maven.apache.org/SETTINGS/1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <profiles>
        <profile>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <repositories>
                <repository>
                    <releases>
                        <enabled>false</enabled>
                    </releases>
                    <snapshots>
                        <enabled>true</enabled>
                    </snapshots>
                    <id>oss-snapshots</id>
                    <url>https://maven.mitro.dkfz.de/oss-snapshots</url>
                </repository>

                <repository>
                    <releases>
                        <enabled>true</enabled>
                    </releases>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                    <id>oss-releases</id>
                    <url>https://maven.mitro.dkfz.de/oss-releases</url>
                </repository>
            </repositories>
        </profile>
    </profiles>
</settings>
```
