## Project directory hierarchy for maven

Maven in its standard configuration requires a different directory layout than
"normal" Eclipse or Netbeans projects. With the following project layout maven
does not require a special configuration.

```
./src
    `-- main
       `-- java
          `-- de                   | etc. Java packages that are required at runtime
       `-- webapp                  | this is the WebContent folder from eclipse. All xhtml files should be in here.
          `-- WEB-INF  
             `-- web.xml
             `-- faces-config.xml
             `-- conf              | for configuration files. This folder is also checked by samply.common.config in the fallback mode.
                `-- log4j2.xml     | log4j2 configuration.
          `-- index.xhtml
          `-- resources
             `-- js                | all java script files
             `-- fonts             | all fonts
             `-- css               | all stylesheets
             `-- images            | all images
          `-- admin                | for example...
             `-- index.xhtml       | index for the admin interface
       `-- resources               | files that are come into the jar/war
          `-- bundles              | bundles for JSF
             `-- msg.properties
          `-- sql                  | SQL files for upgrades
          `-- ...
    `-- test                       | Same as ./src/main, but this folder is used for unit tests only. It is not packaged into the jar or war.
       `-- java
          `-- de
./pom.xml
```

With this layout everyone knows where to find different file types.
